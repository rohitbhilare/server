module.exports = {
    getFeedbyPage: async(req, res) => {
        var result = {};
        var data = require("../dataStored/mock_data");
        var pageNo = req.body.pageNumber;
        var pageSize = req.body.pageSize;
        if (req.body.keyword) {
            var keyword = req.body.keyword.toLowerCase();
            if (keyword.substr(-1, 1) == '"' && keyword.substr(0, 1) == '"') {
                keyword = keyword.replace(/['"]+/g, "");
                data = data.filter(
                    eachData =>
                    eachData.name.toLowerCase().includes(keyword) ||
                    eachData.description.toLowerCase().includes(keyword)
                );
            } else {
                let KeywordArray = keyword.split(" ");
                let filteredArray = [];
                let cnt = KeywordArray.length;
                data.forEach(eachElement => {
                    var counter = 0;
                    KeywordArray.forEach(element => {
                        if (
                            eachElement.name.toLowerCase().includes(element) ||
                            eachElement.description.toLowerCase().includes(element)
                        )
                            counter++;
                    });
                    if (cnt == counter) {
                        filteredArray.push(eachElement);
                    }
                });
                data = filteredArray;
            }
        }

        data = data.sort(function(a, b) {
            if (req.body.isSortedbyTitle) {
                var nameA = a.name.toLowerCase(),
                    nameB = b.name.toLowerCase();
                if (nameA < nameB) return -1;
                if (nameA > nameB) return 1;
                return 0;
            } else if (req.body.isSortedbyUpdatedDate) {
                var dateA = new Date(a.dateLastEdited),
                    dateB = new Date(b.dateLastEdited);

                return dateA - dateB;
            } else {
                return 0;
            }
        });

        result["data"] = data.slice((pageNo - 1) * pageSize, pageNo * pageSize);
        result["totalcount"] = data.length;
        res.send(result);
    }
};