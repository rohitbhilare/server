const express = require("express");
const router = new express.Router();

const feedController = require("../controllers/feedController");

//Feed
//We can use Post method to getting three params
router.post("/api/feed", feedController.getFeedbyPage);

module.exports = router;