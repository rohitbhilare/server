const app = require("./app");

const http = require("http");
const port = process.env.PORT || "4500";
// const port = "4500";
app.set("port", port);
const server = http.createServer(app);
server.listen(port);